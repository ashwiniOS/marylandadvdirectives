//
//  ViewController.swift
//  Maryland Advance Directives
//
//  Created by Deepu on 11/3/18.
//  Copyright © 2018 Deepu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var verticalStackView: UIStackView!
    
    var allButtons = [UIButton]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let bullet1 = "Home"
        let bullet2 = "Advanced Directives/Living Will Info"
        
        let strings = [bullet1, bullet2]
        
        var fullString = ""
        
        for string: String in strings
        {
            let bulletPoint: String = "\u{2022}"
            let formattedString: String = "\(bulletPoint)\(string)\n"
            
            fullString = fullString + formattedString
        }
        

        
        for case let horizontalStackView as UIStackView in verticalStackView.arrangedSubviews{
            for case let button as UIButton in horizontalStackView.arrangedSubviews{
                
                allButtons.append(button)
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

