//
//  PDFDocViewController.swift
//  Maryland Advance Directives
//
//  Created by Ashwin Nooli on 18/11/18.
//  Copyright © 2018 Deepu. All rights reserved.
//

import UIKit

import PDFKit

class PDFDocViewController: UIViewController {

    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var PDFDisplayView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let pdfView = PDFView()
        _ = self.view
        pdfView.translatesAutoresizingMaskIntoConstraints = false
        PDFDisplayView.addSubview(pdfView)
        
        pdfView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        pdfView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        pdfView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,
                                     constant: 40).isActive = true
        pdfView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
        pdfView.autoScales = true
        guard let path = Bundle.main.url(forResource: "adirective", withExtension: "pdf") else { return }
        
        if let document = PDFDocument(url: path) {
            pdfView.document = document
        }
        
        
    }
    
    
    @IBAction func dismissPDF(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
